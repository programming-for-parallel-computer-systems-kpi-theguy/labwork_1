----------------PfPCS--------------------
-------------Labwork #1------------------
-----------Ada. Semaphores---------------
-----------------------------------------
----Task: MR = max(Z)*MO+a*MT*MK---------
-----------------------------------------
--Author: Butskiy Yuriy, IO-52 group-----
--Date: 21.02.2018-----------------------
-----------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

package body data is
	
procedure Input_Integer(a: out Integer) is

begin
      a := 1;
end Input_Integer;

procedure Input_Vector(A: out Vector) is

begin
      for i in 1..N loop
         A(i):=1;
      end loop;
end Input_Vector;

procedure Input_Matrix(MA: out Matrix) is

begin
      for i in 1..N loop
         for j in 1..N loop
            MA(i)(j):=1;
         end loop;
      end loop;
end Input_Matrix;

procedure Output_Matrix(MA: in Matrix) is

begin
      if (N < 10) then
         New_Line;
         for i in 1..N loop
            for j in 1..N loop
               Put(MA(i)(j));
            end loop;
            New_Line;
         end loop;
         New_Line;
      end if;

end Output_Matrix;

function Multiply_Matrixes(MA, MB: in Matrix; H: in integer) return Matrix is
   	cell: Integer;
	result: Matrix;
begin
	for i in 1+H..(N/2)+H loop
		for j in 1..N loop
			cell := 0;
			for k in 1..N loop
				cell := Cell + MA(i)(k) * MB(k)(j);
			end loop;
			result(i)(j) := cell;
		end loop;
	end loop;
	return result;
end Multiply_Matrixes;

function Multiply_Matrix_Integer(MA: in Matrix; a,H: in integer) return Matrix is
	result: Matrix;
begin
	for i in 1+H..(N/2) + H loop
		for j in 1..N loop
			Result(i)(J) := a * MA(I)(J);
		end loop;
	end loop;
	return result;
end Multiply_Matrix_Integer;

procedure Sum_Matrixes(MA, MB: in Matrix; MC: out Matrix; H: in Integer) is
	
begin
	for i in 1 + H..(N/2) + H loop
		for j in 1..N loop
			MC(i)(j) := MA(i)(j) + MB(i)(j);
		end loop;
	end loop;
end Sum_Matrixes;

function Max_Vector(A: in Vector; H: in Integer) return Integer is
	result: integer;
begin
	result := A(1 + H);
	for i in 1 + H..(N/2) + H loop
		if A(i) > result then
			result := A(i);
		end if;
	end loop;
	return result;
end Max_Vector;

end data;
