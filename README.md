Laboratory work #1 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Semaphores mechanism, that was made in program language Ada.
Programming language: Ada
Used technologies: From the standard library I have used special package named Ada.Synchronous_Task_Control that works as Binary Semaphore mechanism of interaction between the threads.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.

Program works for 2 threads, first one outputs the result.