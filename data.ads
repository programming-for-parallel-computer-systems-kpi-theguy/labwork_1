----------------PfPCS--------------------
-------------Labwork #1------------------
-----------Ada. Semaphores---------------
-----------------------------------------
----Task: MR = max(Z)*MO+a*MT*MK---------
-----------------------------------------
--Author: Butskiy Yuriy, IO-52 group-----
--Date: 21.02.2018-----------------------
-----------------------------------------
generic
	N: in Integer;
	package data is
		type Vector is array (1..N) of Integer;
		type Matrix is array (1..N) of Vector;

		--Input Integer, Vector, Matrix
		procedure Input_Vector (A: out Vector);
		procedure Input_Matrix (MA: out Matrix);
		procedure Input_Integer(a: out Integer);

		--Output Matrix
		procedure Output_Matrix (MA: in Matrix);

		--Multiplication functions
		function Multiply_Matrixes(MA, MB: in Matrix; H: in integer) return Matrix;
 	    function Multiply_Matrix_Integer(MA: in Matrix; a,H: in integer) return Matrix;

		--Sum function
		procedure Sum_Matrixes(MA, MB: in Matrix; MC: out Matrix; H: in Integer);

		--Max function
		function max_Vector(A: in Vector; H: in Integer) return Integer;

		end data;
