----------------PfPCS--------------------
-------------Labwork #1------------------
-----------Ada. Semaphores---------------
-----------------------------------------
----Task: MR = max(Z)*MO+a*MT*MK---------
-----------------------------------------
--Author: Butskiy Yuriy, IO-52 group-----
--Date: 21.02.2018-----------------------
-----------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Synchronous_Task_Control;
use Ada.Text_IO, Ada.Integer_Text_IO, Ada.Synchronous_Task_Control;
with data;

procedure Lab1 is

   N: Integer := 2000;

   package using_data is new data(N);
   use using_data;

   b: Integer := 1;	
   a: Integer;
   Z: Vector;
   MO, MT, MK, MR: Matrix;

   S0_1,S0_2,S1,S2,S3,S4: Suspension_Object;

   procedure tasks is

      task T1 is
         --pragma Task_Name("T1");
         pragma Storage_Size(1000000);
         pragma Priority(7);
      end T1;

      task body T1 is
	  	H: integer := 0;
	  	a1, b1: integer;
		MT1: Matrix;
      begin
         Put_Line("T1 started");

		 --Input values
		 Input_Integer(a);
		 Input_Vector(Z);
		 Input_Matrix(MO);
		 Input_Matrix(MT);
		 Input_Matrix(MK);

		 --Notify T2 about input
		 Set_True(S1);

		 --Calculate local max
		 b1 := Max_Vector(Z, H);

		 --Calculate global max
		 Suspend_Until_True(S0_1);
		 if b1 > b then
		 	b := b1;
		 end if;
		 Set_True(S0_1);

		 --Notify T2 about calculate
		 Set_True(S2);
		 --Wait calculating in T2
		 Suspend_Until_True(S3);

		 --Copying critical values
		 Suspend_Until_True(S0_2);
		 a1 := a;
		 b1 := b;
		 MT1 := MT;
		 Set_True(S0_2);

		 --Calculating function
		 Sum_Matrixes(Multiply_Matrix_Integer(MO, b1, H), Multiply_Matrix_Integer(Multiply_Matrixes(MK, MT1, H), a1, H), MR, H);

		 --Wait calculating in T2
		 Suspend_Until_True(S4);

		 --Output result
		 Output_Matrix(MR);

         Put_Line("T1 finished");
      end T1;

      task T2 is
         --pragma Task_Name("T2");
         pragma Storage_Size(1000000);
         pragma Priority(7);
      end T2;

      task body T2 is
	  	H: integer := N/2;
	    a2, b2: Integer;
		MT2: Matrix;
      begin
         Put_Line("T2 started");

		 --Wait input in T1
		 Suspend_Until_True(S1);

		 --Calculate local max
		 b2 := Max_Vector(Z, H);

		 --Calculate global max
		 Suspend_Until_True(S0_1);
		 if b2 > b then
		 	b := b2;
		 end if;
		 Set_True(S0_1);

		 --Notify T1 about calculate
		 Set_True(S3);
		 --Wait calculating in T1
		 Suspend_Until_True(S2);

		 --Copying critical values
		 Suspend_Until_True(S0_2);
		 a2 := a;
		 b2 := b;
		 MT2 := MT;
		 Set_True(S0_2);

		 --Calculating function
		 Sum_Matrixes(Multiply_Matrix_Integer(MO, b2, H), Multiply_Matrix_Integer(Multiply_Matrixes(MK, MT2, H), a2, H),MR, H);

		 --Notify T1 about calculate
		 Set_True(S4);

         Put_Line("T2 finished");
      end T2;

   begin
      null;
   end tasks;

begin
	Set_True(S0_1);
	Set_True(S0_2);
	tasks;
end Lab1;
